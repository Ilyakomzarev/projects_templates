#pragma once
#ifdef _MSC_VER
#pragma warning(disable : 4251)
#endif
// clang-format off
#ifndef XXX_LIBRARY_STATIC
#if defined(_MSC_VER) || defined(WIN64) || defined(_WIN64) || defined(__WIN64__) || defined(WIN32) || defined(_WIN32) || defined(__WIN32__) || defined(__NT__)
#    ifdef XXX_LIBRARY
#        define XXX_EXPORT __declspec(dllexport)
#    else
#        define XXX_EXPORT __declspec(dllimport)
#    endif
#else
#    define XXX_EXPORT  __attribute__((visibility("default")))
#endif
#else
#   define XXX_EXPORT
#endif


