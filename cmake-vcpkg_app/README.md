## Xxx

* Install vcpkg `git clone https://github.com/microsoft/vcpkg && ./vcpkg/bootstrap-vcpkg.sh`
* Install dependances `./vcpkg/vcpkg install catch2 fmt`
* Put ``` "cmake.configureSettings": { "CMAKE_TOOLCHAIN_FILE": "C:/vcpkg/vcpkg/scripts/buildsystems/vcpkg.cmake"}, ```
  to setting.json. Where path is output of `vcpkg integrate install`.
* Or manually build from console `cmake .. -DCMAKE_TOOLCHAIN_FILE=/path/to/vcpkg/scripts/buildsystems/vcpkg.cmake && make`
