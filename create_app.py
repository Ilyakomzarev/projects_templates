import os
import shutil
import sys
from argparse import ArgumentParser
parser = ArgumentParser()
parser.add_argument("-f", "--folder", help="template folder",required=True)
parser.add_argument("-n", "--name", help="project name", required=True)

args = parser.parse_args()
dest_dir = os.path.join(os.path.abspath(os.curdir), args.name)
shutil.copytree(args.folder, dest_dir)
os.rename(os.path.join(dest_dir, "src/project.cpp"),os.path.join(dest_dir, f"src/{args.name.lower()}.cpp"))

os.rename(os.path.join(dest_dir, "src/project.hpp"),
          os.path.join(dest_dir, f"src/{args.name.lower()}.hpp"))

os.rename(os.path.join(dest_dir, "test/test_project.cpp"),
          os.path.join(dest_dir, f"test/test_{args.name.lower()}.cpp"))

for dirname, dirnames, filenames in os.walk(dest_dir, False):
    if ".vscode" in dirname:
        continue
    if "build" in dirname:
        continue
    for file in filenames:
        if file.startswith("."):
            continue

        with open(os.path.join(dirname, file), 'r') as f:
            filedata = f.read()
        filedata = filedata.replace('xxx', args.name.lower())
        filedata = filedata.replace('Xxx', args.name)
        filedata = filedata.replace('XXX', args.name.upper())
        with open(os.path.join(dirname, file), 'w') as f:
            f.write(filedata)
